import React, { useRef, useState } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import { Link, useHistory } from 'react-router-dom'

function Login() { 
	const emailRef = useRef()
	const passwordRef = useRef()
	const { login } = useAuth()
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)
	const history = useHistory()

	async function handleSubmit(e) {
		e.preventDefault()

		try {
			setError('')
			setLoading(true)
			await login(emailRef.current.value, passwordRef.current.value)
			history.push('/')
		}
		catch {
			setError('Failed to sign in')
		}
		setLoading(false)
	}

	return(
		<>
		<Card>
			<Card.Body>
				<h2>Login</h2>
				{error}
				<Form onSubmit={handleSubmit}>
					<Form.Group id="email">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" ref={emailRef} required />
					</Form.Group>
					<Form.Group id="password">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" ref={passwordRef} autoComplete="on" required />
					</Form.Group>
					<Button disabled={loading} type="submit">Submit</Button>
				</Form>
			</Card.Body>
		</Card>
		<div>
			<Link to='/signup'>Sign up</Link>
		</div>
		</>
	)
}

export default Login;
