import React, { useRef, useState } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useAuth } from '../contexts/AuthContext'
import { Link, useHistory } from 'react-router-dom'

function Signup() { 
	const emailRef = useRef()
	const passwordRef = useRef()
	const repasswordRef = useRef()
	const { signup } = useAuth()
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)
	const history = useHistory()

	async function handleSubmit(e) {
		e.preventDefault()

		if(passwordRef.current.value !== repasswordRef.current.value) {
			return setError('Passwords do not match') 
		}
		try {
			setError('')
			setLoading(true)
			await signup(emailRef.current.value, passwordRef.current.value)
			history.push('/')
		}
		catch {
			setError('Failed to create an account')
		}
		setLoading(false)
	}

	return(
		<>
		<Card>
			<Card.Body>
				<h2>Sign Up</h2>
				{error}
				<Form onSubmit={handleSubmit}>
					<Form.Group id="email">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" ref={emailRef} required />
					</Form.Group>
					<Form.Group id="password">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" ref={passwordRef} autoComplete="on" required />
					</Form.Group>
					<Form.Group id="repassword">
						<Form.Label>Re Password</Form.Label>
						<Form.Control type="password" ref={repasswordRef} autoComplete="on" required />
					</Form.Group>
					<Button disabled={loading} type="submit">Submit</Button>
				</Form>
			</Card.Body>
		</Card>
		<div>
			<Link to='/login'>Log in</Link>
		</div>
		</>
	)
}

export default Signup;
