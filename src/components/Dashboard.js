import React, { useState } from 'react'
import { Card, Button } from 'react-bootstrap'
import { useAuth } from './contexts/AuthContext'
import { Link, useHistory } from 'react-router-dom'

export default function Dashboard() {
	const [error, setError] = useState("")
	const { currentUser, logout } = useAuth()
	const history = useHistory()
	async function handleLogout() {
		setError('')
		try {
			await logout()
			history.push('/login')
		}
		catch {
			setError('Failed to log out')
		}
	}

	return (
		<>
			<Card>
				<Card.Body>
					<h2>Profile</h2>
					{error}
					<strong>Email: </strong> {currentUser.email}
					<Link to='/update-profile'> Update Profile </Link>
					<Link to='/notebook'> Notebook </Link>
				</Card.Body>
			</Card>
			<div>
				<Button onClick={handleLogout}>Logout</Button>
			</div>
		</>
	)
}
