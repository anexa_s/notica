import React from 'react'
import NavbarComponent from './Navbar'
import AddFolderButton from './AddFolderButton'
export default function Notebook() {
	return (
		<div>
		<NavbarComponent></NavbarComponent>
		<AddFolderButton />
		</div>
	)
}
