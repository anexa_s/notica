import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function NavbarComponent() {
	return (
		<Navbar bg='light'>
			<Navbar.Brand as={Link} to="/notebook">
				Notebook
			</Navbar.Brand>
			<Nav>
				<Nav.Link as={Link} to="/">
					Dashboard
				</Nav.Link>
			</Nav>
		</Navbar>
	)
}
