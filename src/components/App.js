import Signup from './auth/Signup'
import Login from './auth/Login'
import Dashboard from './Dashboard'
import { AuthProvider } from './contexts/AuthContext'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import PrivateRoute from './PrivateRoute'
import UpdateProfile from './UpdateProfile'
import Notebook from './notebook/Notebook'
import 'bootstrap/dist/css/bootstrap.css'

function App() {
  return (
	  <div>
		  Hello World!
		  <Router>
			<AuthProvider>
				<Switch>
					<PrivateRoute path="/notebook" component={Notebook} />
					<PrivateRoute exact path="/" component={Dashboard} />
					<PrivateRoute path="/update-profile" component={UpdateProfile} />

					<Route path="/signup" component={Signup} />
					<Route path="/login" component={Login} />
				</Switch>
			</AuthProvider>
		  </Router>
	  </div>
  );
}

export default App;
