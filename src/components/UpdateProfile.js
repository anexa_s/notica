import React, { useRef, useState } from 'react'
import { Card, Button, Form } from 'react-bootstrap'
import { useAuth } from './contexts/AuthContext'
import { Link, useHistory } from 'react-router-dom'

function UpdateProfile() { 
	const emailRef = useRef()
	const passwordRef = useRef()
	const repasswordRef = useRef()
	const { currentUser, updatePassword, updateEmail } = useAuth()
	const [error, setError] = useState('')
	const [loading, setLoading] = useState(false)
	const history = useHistory()

	function handleSubmit(e) {
		e.preventDefault()

		if(passwordRef.current.value !== repasswordRef.current.value) {
			return setError('Passwords do not match') 
		}

		const promises = []
		setError('')
		setLoading(true)

		if(emailRef.current.value !== currentUser.email) {
			promises.push(updateEmail(emailRef.current.value))
		}
		if(passwordRef.current.value) {
			promises.push(updatePassword(passwordRef.current.value))
		}

		Promise.all(promises).then(() => {
			history.push('/')
		}).catch(() => {
			setError('Failed to update account')
		}).finally(() => {
			setLoading(false)
		})

	}

	return(
		<>
		<Card>
			<Card.Body>
				<h2>Update Profile</h2>
				{error}
				<Form onSubmit={handleSubmit}>
					<Form.Group id="email">
						<Form.Label>Email</Form.Label>
						<Form.Control type="email" ref={emailRef} required defaultValue={ currentUser.email }/>
					</Form.Group>
					<Form.Group id="password">
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" ref={passwordRef} autoComplete="on" placeholder="Leave blank to keep the same" />
					</Form.Group>
					<Form.Group id="repassword">
						<Form.Label>Re Password</Form.Label>
						<Form.Control type="password" ref={repasswordRef} autoComplete="on" placeholder="Leave blank to keep the same" />
					</Form.Group>
					<Button disabled={loading} type="submit">Submit</Button>
				</Form>
			</Card.Body>
		</Card>
		<div>
			<Link to='/'>Cancel</Link>
		</div>
		</>
	)
}

export default UpdateProfile;
